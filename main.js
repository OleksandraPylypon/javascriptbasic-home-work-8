let paragraphs = document.getElementsByTagName("p");
for (let i = 0; i < paragraphs.length; i++) {
  paragraphs[i].style.backgroundColor = "#ff0000";
}

const idList = document.getElementById("optionsList");
console.log(idList);
const parentElement = idList.parentNode;
console.log(parentElement);

const childNodes = idList.childNodes;
for (let i = 0; i < childNodes.length; i++) {
  const child = childNodes[i];
  console.log(child.nodeName + " (" + child.nodeType + ")");
}

//в завданні прописано, що це має бути елемент з класом testParagraph, але в документі даного параграфу з таким класом не існує, існує лише параграф з таким id. Тому при виконанні цього завдання виникла помилка в консолі. І щоб виконати це завдання, я зміню пошук елемента по класу на елемент по ID
const testParagraph = document.querySelector("#testParagraph");
testParagraph.textContent = "This is a paragraph";

//Або ж є інший варіант розвитку подій - присвоїти даному параграфу клас із найменуванням testParagraph і вже потім задати йому той контент
const secondTestParagraph = document.getElementById("testParagraph");
secondTestParagraph.classList.add("testParagraph");
secondTestParagraph.textContent = "This is a paragraph";

const mainHeaderElement = document.querySelector(".main-header");
const childElements = mainHeaderElement.querySelectorAll("*");
console.log(childElements);

const navElements = document.querySelectorAll(".main-header *");
navElements.forEach(function (element) {
  element.classList.add("nav-item");
});

const sectionTitle = document.querySelector(".section-title");
sectionTitle.classList.remove("section-title");
